# docksi

[![pipeline status](https://gitlab.com/Kwilco/docksi/badges/master/pipeline.svg)](https://gitlab.com/Kwilco/docksi/commits/master)

Docksi is a wrapper for docker-based CLI images. Why would you want to install
images with Docksi?

* Easy installation of docker-based CLIs on your $PATH.
* Automatically pulls the latest image before every run.
* Automatically mounts volumes and publishes ports from images.

Docksi stands for Docker Script Installer. The name and functionality are
heavily inspired by [pipsi](https://github.com/mitsuhiko/pipsi#pipsi).


# SECURITY WARNING

Running commands with the Docksi wrapper inspects an image for exposed volumes
and ports, and auto-mounts / auto-publishes them onto the host. This means
that a malicious image could craft a volume path that results in Docksi
mounting a sensitive path on the host. It will also mount the host's Docker
socket if the container wishes, which will essentially give it root. It is a
**terrible idea to use Docksi with any image you do not trust 100% to be
friendly**. You have been warned.


## Installation

Docksi requires `bash` and `docker`. Please consult:
https://docs.docker.com/install/

```bash
curl --silent https://gitlab.com/Kwilco/docksi/raw/master/get-docksi.sh | bash -
```

You may also download and run the script after inspecting it.

## Path

For convenience, it is highly recommended to put `~/.local/docksi-bin` on your
shell's $PATH, so that scripts installed by Docksi can be run more easily.
Instructions are provided upon running the installer, but they are repeated here:

* For bash, you can run: `echo "PATH=$PATH:~/.local/docksi-bin">>~/.bashrc`
* For fish, you can run: `set -U fish_user_paths ~/.local/docksi-bin $fish_user_paths`
* For other shells, you can probably figure it out!

## Usage

Run `docksi` to see the latest available commands. The important ones are documented here.

`docksi install`

This installs a docker image. The name of the command will be the name of the
docker image. Example:

```bash
$ docksi install example/docker-image
$ docker-image --help
```

Any image can be installed via Docksi, but it may or may not work depending on
how the image is made.

`docksi list`

Lists commands installed with Docksi.

`docksi uninstall`

Removes a command installed with docksi. Example:
```bash
$ docksi uninstall esv-gitlab-sync
```

## Docksi volume mounting and port publishing

If an installed images exposes volumes or ports, Docksi includes the
appropriate arguments in the `docker run` invocation to mount or publish
them.

The convention for volumes is: The volume path from the image is prefixed with
the user's home directory and mounted on the host. For instance, if the
exposed volume in an image is `/.vault-token`, then Docksi will produce the
following `docker run` flags:

```bash
--volume ~/.vault-token:/.vault-token
```

Docksi publishes any ports specifies in the images. For instance, "80/tcp" in
the container image results in the following flags:

```bash
--publish 80:80/tcp
```

To see what volume and port flags would be run for a particular image, you can
pass the `--docksi-dry-run` flag to any command that you have installed. That
does not actually execute the image, but instead displays what volume and port
flags would be passed to it during an actual run. Note that since images will
be automatically updated, these are subject to change at any time at the whims
of the image maintainer.

```bash
$ docksi install example/docker-image --docksi-dry-run
```

In cases where a container used by docksi needs to run docker commands, the local
docker socket can be mounted in by setting a VOLUME in your Dockerfile to
`/var/run/docker.sock`. It is important to note that this is a special case in docksi
where something outside of the "$HOME" directory can be mounted.

## Uninstallation

If want to remove `docksi`, you can remove it in one of the following ways:
* `docksi uninstall docksi`
* `~/.local/docksi-bin/docksi uninstall docksi`
* `rm -rf ~/.local/docksi-bin`
